<?php
/**
 * 
 */
class Allmn_Banners_Block_Adminhtml_Banners_Renderer_Banner extends Varien_Data_Form_Element_Image
{
	
	/**
     * Return element html code
     *
     * @return string
     */
    public function getElementHtml()
    {
        $html = '';

        if ((string)$this->getValue()) {
            $url = $this->_getUrl();

            if( !preg_match("/^http\:\/\/|https\:\/\//", $url) ) {
                $url = Mage::getBaseUrl('media') . $url;
            }

            $html = '<a href="' . $url . '"'
                . ' onclick="imagePreview(\'' . $this->getHtmlId() . '_image\'); return false;">'
                . '<img src="' . $url . '" id="' . $this->getHtmlId() . '_image" title="' . $this->getValue() . '"'
                . ' alt="' . $this->getValue() . '" height="200" width="200" class="small-image-preview v-middle" />'
                . '</a> <br><br>';
        }
        $this->setClass('input-file');
        $html .= Varien_Data_Form_Element_Abstract::getElementHtml();
        $html .= $this->_getDeleteCheckbox();

        return $html;
    }
}