<?php
/**
 * 
 */
class Allmn_Banners_Model_Items extends Mage_Core_Model_Abstract{
	/**
	 * @method __construct
	 *
	 * @return
	 */
	public function __construct(){
		parent::__construct();
		$this->_init('allmnbanners/items');
	}

	/**
	 * Remove images from given group ID
	 * 
	 * @param integer $id - Group ID
	 * @return void
	 */
	public function removeImages($id = NULL){
		if( empty($id) ) return;

		$images = Mage::getModel('allmnbanners/items')->getCollection()->addFieldToFilter('group_id', array('eq' => $id));

		foreach( $images as $image ) $image->delete();

		return;
	}

	/**
	 * Delete images files from given group ID
	 * 
	 * @param integer $id - Group ID
	 * @return void
	 */
	public function deleteFiles($id = NULL){
		$images = Mage::getModel('allmnbanners/items')->getCollection()->addFieldToFilter('group_id', array('eq' => $id));

		foreach( $images as $image ) unlink(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA).DS.'allmnbanners'.DS.$image->getImage());
	}

	/**
	 * Delete given file
	 * 
	 * @param string $file - Filename
	 * @return void
	 */
	public function deleteFile($file = NULL){
		if( empty($file) ) return;


		 unlink(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA).DS.'allmnbanners'.DS.$file);
	}

	// @TODO: Write function description
	public function hasUrl(){
		$url = $this->url;
		
		return !empty($url);
	}
}