<?php
/*
 *
 */
class Allmn_Banners_Block_Adminhtml_Banners_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct(){
		parent::__construct();
		$this->setId('bannersGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(TRUE);
	}

	protected function _prepareCollection(){
		$collection = Mage::getModel('allmnbanners/groups')->getCollection();

		$this->setCollection($collection);

		return parent::_prepareCollection();
	}

	protected function _prepareColumns(){
		$this->addColumn('group_id', array(
			'header' => 'ID',
			'align'  => 'left',
			'width'  => '50px',
			'index'  => 'group_id'
		));

		$this->addColumn('name', array(
			'header' => 'Nome',
			'align'  => 'left',
			'width'  => '120px',
			'index'  => 'name'
		));

		$this->addColumn('identifier', array(
			'header' => 'Identificador',
			'align'  => 'left',
			'width'  => '80px',
			'index'  => 'identifier'
		));

		$this->addColumn('status', array(
			'header'   => 'Status',
			'align'	   => 'left',
			'width'    => '100px',
			'index'    => 'status',
			'renderer' => 'Allmn_Banners_Block_Adminhtml_Banners_Renderer_Status'
		));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row){
		return $this->getUrl('*/*/edit', array('id' => $row->getGroupId()));
	}
}
?>