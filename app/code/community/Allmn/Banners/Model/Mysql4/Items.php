<?php
/**
 * 
 */
class Allmn_Banners_Model_Mysql4_Items extends Mage_Core_Model_Mysql4_Abstract{
	/**
	 * @method __construct
	 *
	 * @return
	 */
	public function _construct(){
		$this->_init('allmnbanners/items', 'item_id');
	}
}