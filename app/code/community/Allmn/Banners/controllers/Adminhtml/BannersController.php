<?php
	class Allmn_Banners_Adminhtml_BannersController extends Mage_Adminhtml_Controller_Action{
		/**
		 * Set breadcrumbs and mark active menu
		 * 
		 * @return void
		 */
		protected function _initAction(){
			$this->loadLayout()
			    ->_setActiveMenu('cms/allmnbanners')
				->_addBreadcrumb(Mage::helper('adminhtml')->__('Banners'), Mage::helper('adminhtml')->__('Banners'));

			return $this;
		}

		/**
		 * Display grid with items
		 * 
		 * @return void
		 */
		public function indexAction(){
			$this->_initAction();
				$this->_addContent($this->getLayout()->createBlock('allmnbanners/adminhtml_grid')); 
			$this->renderLayout();
		}

		/**
		 * Display create form
		 * 
		 * @return void
		 */
		public function newAction(){
			$this->_forward('edit');
		}

		/**
		 * Display create/edit form
		 * 
		 * @return void
		 */
		public function editAction(){
			$id = $this->getRequest()->getParam('id');
			$entry = Mage::getModel('allmnbanners/groups')->load($id);

			if( $entry->getGroupId() || $id == 0 ){
				if( !empty($entry) ) Mage::register('banners_data', $entry);
				
				$this->loadLayout();
				$this->_setActiveMenu('cms/allmnbanners');
				$this->_addBreadcrumb('Gerenciar Banners', 'Gerenciar Banners');
				$this->_addBreadcrumb('Grupos', 'Grupos');
				$this->getLayout()->getBlock('head')
					 ->setCanLoadExtJs(TRUE);
				$this->_addContent(
						$this->getLayout()
							 ->createBlock('allmnbanners/adminhtml_banners_edit')
					 )
					 ->_addLeft(
					 	$this->getLayout()
					 		 ->createBlock('allmnbanners/adminhtml_banners_edit_tabs')
					 );

				$this->renderLayout();
			}else{
				Mage::getSingleton('adminhtml/session')->addError('ID inválida');
				$this->_redirect('*/*/');
			}
		}

		/**
		 * Save banner group
		 * 
		 * @return void
		 */
		public function saveAction(){
			if( $data = $this->getRequest()->getPost() ){
				$groupModel = Mage::getModel('allmnbanners/groups');

				$id = $this->getRequest()->getParam('id');
				if( !empty($id) ) $groupModel->load($id);

				$groupModel->setName($data['name']);
				$groupModel->setStatus($data['status']);
				$groupModel->setIdentifier($data['identifier']);
				
				try{					
					if(isset($data['banner']['delete'])){
						$groupModel->setBanner("");
					}
					elseif(isset($_FILES['banner']['name']) && $_FILES['banner']['name'] != "" ) {

						$uploader = new Varien_File_Uploader('banner');
			            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
			            $uploader->setAllowRenameFiles(TRUE);
			            $uploader->setFilesDispersion(FALSE);
			            $result = $uploader->save(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA).DS.'allmnbanners'.DS.$_FILES['file']['name']);
			            #$result['url'] =  Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'allmnbanners/'.$result['file'];	
						$result['url'] =  'allmnbanners/'.$result['file'];	
						$groupModel->setBanner($result['url']);
					}

					$groupModel->save();

				}catch(Exception $e){
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
	                Mage::getSingleton('adminhtml/session')->setFormData($data);
	                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
	                return;
				}

				if( isset($data['images']) && !empty($data['images']) ){
					Mage::getModel('allmnbanners/items')->removeImages($groupModel->getGroupId());
					
					$images = json_decode($data['images']);
					
					foreach( $images as $image ){
						if( !$image->removed ){
							try{
								Mage::getModel('allmnbanners/items')->setGroupId($groupModel->getGroupId())
									->setUrl($image->label)
									->setImage($image->file)
									->setGroup($image->group)
									->setPosition($image->position)
									->setStatus(!$image->disabled)
									->save();
							}catch(Exception $e){
								Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				                Mage::getSingleton('adminhtml/session')->setFormData($data);
				                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				                return;
							}
						}else Mage::getModel('allmnbanners/items')->deleteFile($image->file);
					}
				}

				if ($this->getRequest()->getParam('back')) {
					Mage::getSingleton('adminhtml/session')->addSuccess('Salvo com sucesso');
	                $this->_redirect('*/*/edit', array('id' => $groupModel->getGroupId()));
	                return;
	            }
			}

			Mage::getSingleton('adminhtml/session')->addSuccess('Salvo com sucesso');
			if($this->getRequest()->getParam('back')) $this->_redirect('*/*/new');
            else $this->_redirect('*/*/');
		}

		public function deleteAction(){
			if( $this->getRequest()->getParam('id') > 0 ){
	            try {
	                // Remove files
	                Mage::getModel('allmnbanners/items')->deleteFiles($this->getRequest()->getParam('id'));

	            	// Delete entries
	                Mage::getModel('allmnbanners/groups')->load($this->getRequest()->getParam('id'))->delete();

	                Mage::getSingleton('adminhtml/session')->addSuccess('Slider excluído com sucesso!');
	                $this->_redirect('*/*/');
	            } catch (Exception $e) {
	                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
	                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
	            }
	        }

	        $this->_redirect('*/*/');
		}

		/**
		 * Receive file to save
		 * 
		 * @return Zend_Json - JSON encoded response
		 */
		public function uploadAction(){
	        $result = array();
	        try {
	            $uploader = new Varien_File_Uploader('image');
	            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
	            $uploader->setAllowRenameFiles(TRUE);
	            $uploader->setFilesDispersion(FALSE);
	            $result = $uploader->save(Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA).DS.'allmnbanners'.DS.$_FILES['file']['name']);

	            $result['url'] =  Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'allmnbanners/'.$result['file'];
	            $result['cookie'] = array(
	                    'name'     => session_name(),
	                    'value'    => $this->_getSession()->getSessionId(),
	                    'lifetime' => $this->_getSession()->getCookieLifetime(),
	                    'path'     => $this->_getSession()->getCookiePath(),
	                    'domain'   => $this->_getSession()->getCookieDomain()
	            );
	        } catch (Exception $e) {
	            $result = array('error'=>$e->getMessage(), 'errorcode'=>$e->getCode());
	        }

	        $this->getResponse()->setBody(Zend_Json::encode($result));
	    }
	}
?>