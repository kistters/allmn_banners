<?php
/*
 *
 */
class Allmn_Banners_Block_Adminhtml_Banners_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	
	public function __construct(){
		parent::__construct();

		$this->setId('form_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle('Informações');
	}

	protected function _beforeToHtml(){
		$this->addTab('info_section', array(
			'label'   => 'Informações Gerais',
			'title'   => 'Informações Gerais',
			'content' => $this->getLayout()->createBlock('allmnbanners/adminhtml_banners_edit_tab_info')->toHtml()
		));

		$this->addTab('image_section', array(
			'label' => 'Imagens',
			'title' => 'Imagens',
			'content' => $this->getLayout()->createBlock('allmnbanners/adminhtml_banners_edit_tab_image')->toHtml()
		));

		return parent::_beforeToHtml();
	}
}
?>