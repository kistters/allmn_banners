<?php
/**
 * 
 */
class Allmn_Banners_Block_Banners extends Mage_Core_Block_Template
{
	
	public $_banner = null;

	public $_images = null;

	public function _construct()
	{
		parent::_construct();		
		$this->setTemplate('allmnbanners/banners.phtml');
	}

	public function _prepareBannerAndImages(){
		return $this->_loadBannerAndImagens();
	}
	/*
	 * Data passaed by block or controller, like parameter
	 */
	protected function _loadBannerAndImagens()	{
		$identifier = $this->getData('identifier');
		$model = $this->_getBannersModel();
		$this->_banner = $model->loadBannerByIdentifier($identifier);
		$this->_images = $model->getImagens();

		/* fazer uma validação caso não tenha carregado o banner */
		return true;
	}

	public function getBannerAndImagesInfo(){
		$info = array();
		$info['banner'] = $this->_banner;
		$info['images'] = $this->_images;
		return $info;
	}
	protected function _getBannersModel(){
	    return Mage::getModel('allmnbanners/groups');
	}

	protected function _getBannersItemsModel(){
	    return Mage::getModel('allmnbanners/groups');
	}

	public function getImages($file){
		return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'allmnbanners/'.$file;
	}
	public function getBannerImage($file){
		return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).$file;
	}
}