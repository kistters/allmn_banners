<?php
/**
 * 
 */
class Allmn_Banners_Model_Groups extends Mage_Core_Model_Abstract{
	/**
	 * @method __construct
	 *
	 * @return
	 */
	public function __construct(){
		parent::__construct();
		$this->_init('allmnbanners/groups');
	}

	public function getImagens(){
		$imagens = Mage::getModel('allmnbanners/items')->getCollection();
		$imagens->addFieldToFilter('status', array('eq' => 1));
		$imagens->addFieldToFilter('group_id', array('eq' => $this->getId()));
		$imagens->setOrder('position', 'asc')->load();

		return $imagens;
	}

	/*
	 * executa o load do banner caso não exitas o identifier, retorna um objeto vazio
	 */
	public function loadBannerByIdentifier($identifier){
		return  $this->load($identifier, 'identifier');
	}

	/*
	 * retorna o status setado no Admin
	 */
	protected function isActive(){
		return $this->getStatus();
	}
}